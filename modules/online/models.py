# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Asistencia(models.Model):
    TIPO_CHOICES = (("Asistente", "Asistente"),
                    ("Inasistente J", "Inasistente J"),
                    ("Inasistente I", "Inasistente I"),
                    ("Retardo", "Retardo"))
    fecha = models.DateField()
    tipo = models.CharField(max_length=10,
                            choices=TIPO_CHOICES,
                            blank=False, null=False)

    class Meta:
        verbose_name = "Asistencia"
        verbose_name_plural = "Asistencia"
        db_table = 'b_Asistencia'

    def __unicode__(self):
        return "%s - %s" % (self.tipo, self.fecha)
