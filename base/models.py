# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from modules.online.models import Asistencia


class Imagen(models.Model):
    nombre = models.CharField(max_length=500)
    url = models.CharField(max_length=500)

    class Meta:
        verbose_name = "Imagen"
        verbose_name_plural = "Imagenes"
        db_table = 'b_imagen'

    def __unicode__(self):
        return "%s - %s" % (self.nombre, self.url)


class Persona(models.Model):
    TIPO_CHOICES = (("Docente", "Docente"),
                    ("Administrativo", "Administrativo"),
                    ("Obrero", "Obrero"))
    usuario = models.OneToOneField(User, unique=True)
    cedula = models.IntegerField()
    direccion = models.TextField()
    telefono = models.CharField(max_length=50)
    imagen = models.ForeignKey(Imagen, blank=True, null=True)
    tipo = models.CharField(max_length=10,
                            choices=TIPO_CHOICES,
                            blank=False, null=False)
    estado = models.BooleanField(default=True)
    asistencias = models.ManyToManyField(Asistencia)

    class Meta:
        verbose_name = "Persona"
        verbose_name_plural = "Personas"
        db_table = 'b_persona'

    def __unicode__(self):
        return "%s - %s" % (self.usuario.first_name, self.usuario.last_name)
