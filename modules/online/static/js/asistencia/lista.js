$(function (){
	$(".select2").select2();
    $('#lista-asistencia').DataTable({
        //"pagingType": "full_numbers",
        "iDisplayLength": 10,
        'language': language,
        //"dom": 'T<"clear">lfrtip',
	     /*
	      "tableTools": {
	            "sSwfPath": "/static/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
	        },
	        */
    });

    $('.options-select').on('change', function(event) {
    	data = [];
    	data.push({name: 'id_persona', value: $(this).data('person')});
    	data.push({name: 'asistencia', value: $(this).val()});
    	data.push({name: 'csrfmiddlewaretoken', value: $('input[name="csrfmiddlewaretoken"]').val()});
    	$.ajax({
    		url: '/online/registrar_asistencias_ajax/',
    		type: 'POST',
    		dataType: 'json',
    		data: data,
    	})
    	.done(function() {
    		console.log("success");
    	})
    	.fail(function() {
    		console.log("error");
    	});
    	
    });
});