# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-05-18 23:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('base', '0004_persona_estado'),
    ]

    operations = [
        migrations.CreateModel(
            name='Asistencia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField()),
                ('tipo', models.CharField(choices=[('Asistente', 'Asistente'), ('Inasistente', 'Inasistente'), ('Justificado', 'Justificado')], max_length=10)),
                ('persona', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='base.Persona')),
            ],
            options={
                'db_table': 'b_Asistencia',
                'verbose_name': 'Asistencia',
                'verbose_name_plural': 'Asistencia',
            },
        ),
    ]
