# -*- coding: utf-8 -*-
from django import template

from base.models import Persona
from django.utils.html import format_html
from django.utils import timezone


register = template.Library()


@register.simple_tag(takes_context=True)
def get_user_image(context):
    try:
        op = Persona.objects.get(usuario_id=context.request.user.id)
        if op.imagen:
            data = "'/" + unicode(op.imagen.name + "'")
            data = data.replace("\\", "/")
        else:
            data = "img/gravatar.png"
    except:
        data = "img/gravatar.png"
    return data


@register.simple_tag
def get_user_select(persona, fecha):
    now = timezone.localtime(timezone.now())
    exist = persona.asistencias.filter(fecha=now).exists()
    if exist:
        tipo = persona.asistencias.filter(fecha=now)[0].tipo
    else:
        tipo = ''
    data = ""
    data += ('<select data-placeholder="..." name="select_' + str(persona.id) +
             '" data-person=' + str(persona.id) +
             ' class="select2 options-select">')
    data += '<option></option>'

    data += ('<option '+('selected' if 'Asistente' in tipo else '') +
             ' value="Asistente">Asistente</option>')
    data += ('<option '+('selected' if 'Inasistente J' in tipo else '') +
             ' value="Inasistente J">Inasistente J</option>')
    data += ('<option '+('selected' if 'Inasistente I' in tipo else '') +
             ' value="Inasistente I">Inasistente I</option>')
    data += ('<option '+('selected' if 'Retardo' in tipo else '') +
             ' value="Retardo">Retardo</option>')
    data += '</select>'
    return format_html(data)
