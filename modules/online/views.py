from django.views.generic import TemplateView
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User, Group
from base.models import Persona
from modules.online.models import Asistencia
from django.db.models import Q
from django.utils import timezone
from datetime import timedelta
import calendar
from django_datatables_view.base_datatable_view import BaseDatatableView


class NuevaPersona(TemplateView):
    template_name = "nueva_persona.html"

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        user = User(
                first_name=request.POST.get('nombre'),
                last_name=request.POST.get('apellido'),
                username=request.POST.get('cedula'),
                email=request.POST.get('correo'),
            )
        user.save()
        user.set_password('admin')
        user.groups.add(Group.objects.get(name=request.POST.get('perfil')))
        person = Persona(
                usuario=user,
                cedula=request.POST.get('cedula'),
                direccion=request.POST.get('direccion'),
                telefono=request.POST.get('telefono'),
                tipo=request.POST.get('tipo')
            )
        person.save()

        return render_to_response(self.template_name, {},
                                  RequestContext(request))


class ActualizarPersona(TemplateView):
    template_name = "actualizar_persona.html"

    def get(self, request, *args, **kwargs):
        persona = Persona.objects.get(pk=int(kwargs.get('id')))
        try:
            group = persona.usuario.groups.all()[0].name
        except:
            group = ''
        return render_to_response(self.template_name,
                                  {'persona': persona,
                                   'group': group},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        person = Persona.objects.get(pk=int(kwargs.get('id')))
        person.usuario.first_name = request.POST.get('nombre')
        person.usuario.last_name = request.POST.get('apellido')
        person.usuario.username = request.POST.get('cedula')
        person.usuario.email = request.POST.get('correo')
        person.usuario.save()
        person.usuario.set_password('admin')
        person.usuario.groups.clear()
        person.usuario.groups.add(Group.objects.get(name=request.POST.get('perfil')))
        person.cedula = request.POST.get('cedula')
        person.tipo = request.POST.get('tipo')
        person.direccion = request.POST.get('direccion')
        person.telefono = request.POST.get('telefono')
        person.estado = True if request.POST.get('estado') else False
        person.save()
        return HttpResponseRedirect(reverse('index-persona'))


class IndexPersona(TemplateView):
    template_name = "index_persona.html"

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))


class ListaPersonaAjax(BaseDatatableView):
    model = Persona
    columns = ['id', 'usuario.first_name', 'usuario.last_name', 'cedula',
               'usuario.email', 'telefono', 'tipo', 'estado']
    order_columns = ['id', 'usuario.first_name', 'usuario.last_name', 'cedula',
                     'usuario.email', 'telefono', 'tipo', 'estado']

    def get_initial_queryset(self):
        return Persona.objects.order_by('usuario__first_name',
                                        'usuario__last_name')

    def prepare_results(self, qs):
        json_data = []
        for item in qs:
            json_data.append([
                '<a id="' + str(item.id) +
                '" class="edit" href="../actualizar/' +
                str(item.id) + '/" >' +
                '<i class="fa fa-pencil-square-o fa_color"></i></a></div>' +
                '</a>',
                str(item.id),
                item.usuario.first_name,
                item.usuario.last_name,
                item.cedula,
                item.tipo,
                'Activo' if item.estado else 'Inactivo',
            ])
        return json_data

    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(Q(usuario__username__istartswith=search) |
                           Q(usuario__email__istartswith=search) |
                           Q(usuario__first_name__istartswith=search) |
                           Q(usuario__last_name__istartswith=search))
        return qs


class Index(TemplateView):
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name,
                                  {},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))


class RegistrarAsistencias(TemplateView):
    template_name = "registrar_asistencias.html"

    def get(self, request, *args, **kwargs):
        query = {'estado': True}
        if 'personal-obrero' in kwargs.get('tipo'):
            query['tipo'] = 'Obrero'
        elif 'docentes' in kwargs.get('tipo'):
            query['tipo'] = 'Docente'
        elif 'personal-administrativo' in kwargs.get('tipo'):
            query['tipo'] = 'Administrativo'
        personas = Persona.objects.filter(**query)
        fecha = timezone.localtime(timezone.now())
        return render_to_response(self.template_name,
                                  {'personas': personas,
                                   'fecha': fecha},
                                  RequestContext(request))

    def post(self, request, *args, **kwargs):
        return render_to_response(self.template_name, {},
                                  RequestContext(request))


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days) + 1):
        yield start_date + timedelta(days=n)


class Reportes(TemplateView):
    template_name = "reportes_fechas.html"

    def get(self, request, *args, **kwargs):
        if 'semanal' in kwargs.get('tipo', ''):
            lista = []
            fecha = timezone.localtime(timezone.now())
            lunes = fecha.today() - timedelta(days=fecha.today().weekday())
            viernes = lunes + timedelta(days=4)
            personas = Persona.objects.filter(estado=True, tipo='Docente')
            for persona in personas:
                dicc = {}
                dicc['persona'] = persona
                sist = []
                asistencias = persona.asistencias.all()
                for date_range in daterange(lunes, viernes):
                    if asistencias.filter(fecha=date_range).exists():
                        sist.append(asistencias.get(fecha=date_range).tipo)
                    else:
                        sist.append('No Registrado')
                dicc['asistencia'] = sist
                lista.append(dicc)
            return render_to_response(self.template_name,
                                      {'lista': lista},
                                      RequestContext(request))
        elif 'mensual' in kwargs.get('tipo', ''):
            lista = []
            fecha = timezone.localtime(timezone.now())
            primer_dia = timezone.datetime.strptime(
                str(01) + '-' + str(fecha.month) + '-' + str(fecha.year), '%d-%m-%Y')
            ultimo_dia = timezone.datetime.strptime(str(calendar.monthrange(fecha.year-1, fecha.month-1)[1]) +
                                                    '-' + str(fecha.month) + '-' + str(fecha.year), '%d-%m-%Y')
            dias = []
            for date_range in daterange(primer_dia, ultimo_dia):
                dias.append(date_range)
            personas = Persona.objects.filter(estado=True, tipo='Docente')
            for persona in personas:
                dicc = {}
                dicc['persona'] = persona
                sist = []
                asistencias = persona.asistencias.all()
                for date_range in daterange(primer_dia, ultimo_dia):
                    if 'Sun' not in calendar.day_name[date_range.weekday()] and 'Sat' not in calendar.day_name[date_range.weekday()]:
                        if asistencias.filter(fecha=date_range).exists():
                            str_append = ''
                            if 'Inasistente J' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'IJ'
                            elif 'Inasistente I' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'II'
                            elif 'Asistente' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'A'
                            elif 'Retardo' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'R'

                            sist.append(str_append)
                        else:
                            sist.append('NR')
                dicc['asistencia'] = sist
                lista.append(dicc)
            return render_to_response(self.template_name,
                                      {'lista': lista,
                                       'dias': dias,
                                       'tipo': kwargs.get('tipo', '')},
                                      RequestContext(request))

    def post(self, request, *args, **kwargs):
        if 'semanal' in kwargs.get('tipo', ''):
            lista = []
            fecha = timezone.localtime(timezone.now())
            lunes = fecha.today() - timedelta(days=fecha.today().weekday())
            viernes = lunes + timedelta(days=4)
            query = {'estado': True}
            if 'docente' in request.POST.get('tipo', ''):
                query['tipo'] = 'Docente'
            elif 'personal-administrativo' in request.POST.get('tipo', ''):
                query['tipo'] = 'Administrativo'
            elif 'obrero' in request.POST.get('tipo', ''):
                query['tipo'] = 'Obrero'
            personas = Persona.objects.filter(**query)
            for persona in personas:
                dicc = {}
                sist = []
                dicc['persona'] = persona
                asistencias = persona.asistencias.all()
                for date_range in daterange(lunes, viernes):
                    if asistencias.filter(fecha=date_range).exists():
                        sist.append(asistencias.get(fecha=date_range).tipo)
                    else:
                        sist.append('No Registrado')
                dicc['asistencia'] = sist
                lista.append(dicc)
            return render_to_response(self.template_name,
                                      {'lista': lista,
                                       'tipo': request.POST.get('tipo', '')},
                                      RequestContext(request))
        elif 'mensual' in kwargs.get('tipo', ''):
            lista = []
            fecha = timezone.localtime(timezone.now())
            primer_dia = timezone.datetime.strptime(
                str(01) + '-' + str(fecha.month) + '-' + str(fecha.year), '%d-%m-%Y')
            ultimo_dia = timezone.datetime.strptime(str(calendar.monthrange(fecha.year-1, fecha.month-1)[1]) +
                                                    '-' + str(fecha.month) + '-' + str(fecha.year), '%d-%m-%Y')
            dias = []
            for date_range in daterange(primer_dia, ultimo_dia):
                dias.append(date_range)
            query = {'estado': True}
            if 'docente' in request.POST.get('tipo', ''):
                query['tipo'] = 'Docente'
            elif 'personal-administrativo' in request.POST.get('tipo', ''):
                query['tipo'] = 'Administrativo'
            elif 'obrero' in request.POST.get('tipo', ''):
                query['tipo'] = 'Obrero'
            personas = Persona.objects.filter(**query)
            for persona in personas:
                dicc = {}
                dicc['persona'] = persona
                sist = []
                asistencias = persona.asistencias.all()
                for date_range in daterange(primer_dia, ultimo_dia):
                    if 'Sun' not in calendar.day_name[date_range.weekday()] and 'Sat' not in calendar.day_name[date_range.weekday()]:
                        if asistencias.filter(fecha=date_range).exists():
                            str_append = ''
                            if 'Inasistente J' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'IJ'
                            elif 'Inasistente I' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'II'
                            elif 'Asistente' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'A'
                            elif 'Retardo' in asistencias.get(fecha=date_range).tipo:
                                str_append = 'R'

                            sist.append(str_append)
                        else:
                            sist.append('NR')
                dicc['asistencia'] = sist
                lista.append(dicc)
            return render_to_response(self.template_name,
                                      {'lista': lista,
                                       'dias': dias,
                                       'tipo': kwargs.get('tipo', '')},
                                      RequestContext(request))


def registrar_asistencias_ajax(request):
    now = timezone.localtime(timezone.now())
    tipo_asistencia = request.POST.get('asistencia')
    persona = Persona.objects.get(pk=request.POST.get('id_persona'))
    if not persona.asistencias.filter(fecha=now).exists():
        persona.asistencias.add(Asistencia.objects.create(
            tipo=tipo_asistencia,
            fecha=now
            ))
    return HttpResponse({},
                        content_type='application/json; charset=utf-8')
